import authenticationStatic.CredentialsStaticService;
import authenticationStatic.Authentication;
import authenticationStatic.PermissionStaticService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

public class AuthTestMockStatic {
    Authentication authentication;

    @BeforeEach
    void setup(){
        authentication = new Authentication();
    }

    @Test
    public void verifLoginCorrecto(){
        String user = "admin";
        String pwd = "qwerqwer";
        boolean esValido = true;

        MockedStatic<CredentialsStaticService> mockedStaticCredenciales = Mockito.mockStatic(CredentialsStaticService.class);
        mockedStaticCredenciales.when(()-> CredentialsStaticService.isValidCredential(user, pwd)).thenReturn(esValido);

        String permission = "CRUD";

        MockedStatic<PermissionStaticService> mockedStaticPermission = Mockito.mockStatic(PermissionStaticService.class);
        mockedStaticPermission.when(()-> PermissionStaticService.getPermission(user)).thenReturn(permission);

        String expectResult = "user authenticated successfully with permission: ["+permission+"]";
        String actualResult = authentication.login(user, pwd);
        Assertions.assertEquals(expectResult, actualResult, "Error, verifLoginCorrecto");

        mockedStaticCredenciales.close();
        mockedStaticPermission.close();
    }


    @Test
    public void verifLoginIncorrecto(){
        String user = "admin";
        String pwd = "qwerqwer";
        boolean esValido = false;

        MockedStatic<CredentialsStaticService> mockedStaticCredenciales = Mockito.mockStatic(CredentialsStaticService.class);
        mockedStaticCredenciales.when(()-> CredentialsStaticService.isValidCredential(user, pwd)).thenReturn(esValido);

        String expectResult = "user or password incorrect";
        String actualResult = authentication.login(user, pwd);
        Assertions.assertEquals(expectResult, actualResult, "Error, verifLoginCorrecto");

        mockedStaticCredenciales.close();
    }

}
