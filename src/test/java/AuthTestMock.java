import authentication.Authentication;
import authentication.CredentialsService;
import authentication.PermissionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class AuthTestMock {
    Authentication authentication;

    CredentialsService credentialsServiceMock = Mockito.mock(CredentialsService.class);
    PermissionService permissionServiceMock = Mockito.mock(PermissionService.class);

    @BeforeEach
    void setup(){
        authentication = new Authentication();
    }

    @Test
    public void pruebaMsgCorrecto(){
        String user = "admin";
        String pwd = "qwerqwer";

        boolean esValido = true;
        Mockito.when(credentialsServiceMock.isValidCredential(user, pwd)).thenReturn(esValido);

        authentication.setCredentialsService(credentialsServiceMock);

        String permission = "CRUD";

        Mockito.when(permissionServiceMock.getPermission(user)).thenReturn(permission);
        authentication.setPermissionService(permissionServiceMock);

        String exceptedResult = "user authenticated successfully with permission: ["+permission+"]";
        String actualResult = authentication.login(user, pwd);

        Assertions.assertEquals(exceptedResult, actualResult, "error!, pruebaMsgCorrecto");

        Mockito.verify(credentialsServiceMock).isValidCredential(user, pwd);
        Mockito.verify(permissionServiceMock).getPermission(user);
    }
    @Test
    public void pruebaMsgIncorrecto(){
        String user = "admin";
        String pwd = "qwerqwer123";

        boolean esValido = false;
        Mockito.when(credentialsServiceMock.isValidCredential(user, pwd)).thenReturn(esValido);

        authentication.setCredentialsService(credentialsServiceMock);

        String expectedResult = "user or password incorrect";
        String actualResult = authentication.login(user, pwd);

        Assertions.assertEquals(expectedResult, actualResult, "error!, pruebaMsgIncorrecto");

        Mockito.verify(credentialsServiceMock).isValidCredential(user, pwd);

    }
}
